import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GarrafonesComponent } from './garrafones.component';

describe('GarrafonesComponent', () => {
  let component: GarrafonesComponent;
  let fixture: ComponentFixture<GarrafonesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GarrafonesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GarrafonesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
