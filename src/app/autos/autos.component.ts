import { Component, OnInit,OnDestroy, DoCheck } from '@angular/core';
import {auto} from '../models/auto';

@Component({
  selector: 'autos',
  templateUrl: './autos.component.html',
  styleUrls: ['./autos.component.css']
})
export class AutosComponent implements OnInit,OnDestroy,DoCheck {
  titulo:String;
  mostrarAutos:boolean;
  public autos: Array<auto>;
  public marcas: String[];
  public color: String;
  public miMarca: String;



  constructor() {
    this.miMarca = ""
    this.titulo = 'Marcas famosas' 
    this.mostrarAutos = true;
    this.autos = [ 
      new auto('Camaro',200,'Chevrolet','Estados Unidos',true),
      new auto('Hoonda Civic',300,'Honda Motors','Corea',false),
      new auto('Versa',500,'Nissan','Japón',true),
      new auto('prueba',200,'Honda Motors','Corea',false)
    ];
    this.marcas = new Array();
    this.color = 'green';

   }

  ngOnInit() {
    console.log(this.autos);
    this.getMarcas();
    console.log(this.marcas);
  }
  ngOnDestroy(){
    this.mostrarAutos = false;
  }
  ngDoCheck(){
 
  }

  getMarcas(){
    this.autos.forEach(x => {
      //onsole.log (this.marcas.indexOf(x.marca))
      if (this.marcas.indexOf(x.marca)<0){
        this.marcas.push(x.marca);
      }
    
    });
  }

  getMarca(){
    if(this.miMarca.length>0){
      alert(this.miMarca);
    }
   else{
     alert ("Ingrese un valor");
   }
  }

  addMarca(){
    console.log(this.miMarca.length);
    if(this.miMarca.length > 0){
      this.marcas.push(this.miMarca);
      this.miMarca = "";
    }else{
      alert("Ingrese un valor");
    }
    
  }

  clearMarca(){
    this.marcas = [];
    this.miMarca = "";
    
  }

  deleteMarca(index){
    this.marcas.splice(index,1);
  }
  

}
