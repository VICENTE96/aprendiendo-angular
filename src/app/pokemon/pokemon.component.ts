import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router'

@Component({
  selector: 'app-pokemon',
  templateUrl: './pokemon.component.html',
  styleUrls: ['./pokemon.component.css']
})
export class PokemonComponent implements OnInit {
public pokemonNombre: string;
public pokemonTipo: string;
  constructor(
    private _route: ActivatedRoute,
    private _router: Router) {
   }

  ngOnInit() {

    this._route.params.subscribe((params: Params)=>{
      this.pokemonNombre = params.nombre;
      this.pokemonTipo = params.tipo;
     console.log(this.pokemonNombre);
     console.log(this.pokemonTipo);

     if(this.pokemonNombre == 'ninguno')
        this._router.navigate(['/home'])
    });
  }
  
  redirigir(){
    this._router.navigate(['/zapatos'])
  }

}
