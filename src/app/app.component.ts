import { Component } from '@angular/core';
import {configuracion} from './models/configuracion';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public title = 'Proyecto Angular';
  public descripcion = '';
  public mostrar_videojuegos = true;
  public mostrar_autos = true;
  public config;

  constructor (){
    this.title = configuracion.titulo;
    this.descripcion = configuracion.descripcion;
    this.config = configuracion;

  }

  ocultarVideojuegos(value){
    this.mostrar_videojuegos = value;
  }
  ocultarAutos(value){
    this.mostrar_autos = value;
  }
}
