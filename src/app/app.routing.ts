//importar modulos del router de angular
import {ModuleWithProviders} from '@angular/core'; //permite trabajar con el router
import {Routes, RouterModule} from '@angular/router';

//Importar componentes
import {HomeComponent} from './home/home.component';
import {MotosComponent} from './motos/motos.component';
import {PokemonComponent} from './pokemon/pokemon.component';
import {VideojuegoComponent} from './videojuego/videojuego.component';
import {AutosComponent} from './autos/autos.component';
import { zapatosComponent } from './zapatos/zapatos.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {ExternoComponent} from './externo/externo.component';
import {GarrafonesComponent} from './garrafones/garrafones.component';

//Array de rutas

const appRoutes: Routes =[
{path: '', component: HomeComponent},
{path: 'home', component: HomeComponent},
{path: 'motos', component: MotosComponent},
{path: 'pokemon', component: PokemonComponent},
{path: 'videojuegos', component: VideojuegoComponent},
{path: 'autos', component: AutosComponent},
{path: 'zapatos', component: zapatosComponent},
{path: 'zapatos/:marcaFav', component: zapatosComponent},
{path: 'pokemon/:nombre/:tipo', component: PokemonComponent},
{path: 'externo', component: ExternoComponent},
{path: 'garrafones', component:GarrafonesComponent},
{path: '**', component: NotFoundComponent},
];

//Exportar el modulo del router
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);