import {Component,OnInit,DoCheck,OnDestroy} from '@angular/core';
import {videojuego} from '../models/videojuego';
import {VideojuegoService} from '../Services/videojuego.service';
@Component({
    selector: 'videojuego',
    templateUrl:'./videojuego.component.html',
    providers: [VideojuegoService] 
})
export class VideojuegoComponent implements OnInit,DoCheck,OnDestroy{
    public titulo: string;
    public listado: string;
    public videojuegos: Array<videojuego>;
   
  constructor(private _videojuegoService : VideojuegoService){
      this.titulo = "Componente de videojuegos"
      this.listado = "Listado de los juegos más populares"
     
  }
  
  ngOnInit(){
   //console.log ("On init ejecutado");
   //console.log (this.videojuegos);
   this.videojuegos = this._videojuegoService.getVideojuegos();
   alert (this._videojuegoService.getTexto());
   console.log(this.videojuegos);

  }
  ngDoCheck(){
      console.log("Do check ejecutado");

  }
   ngOnDestroy(){
   console.log("On destroy ejecutado");
  }

  cambiarTitulo(){
      this.titulo = "Titulo Cambiado";
  }

}

