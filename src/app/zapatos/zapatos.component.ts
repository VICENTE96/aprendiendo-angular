import {Component,OnInit} from '@angular/core'
import {Router, ActivatedRoute, Params} from '@angular/router'

@Component({
selector: 'zapatos',
templateUrl: './zapatos.component.html'
})
export class zapatosComponent implements OnInit {

    public marcaPreferida: string;

    public titulo: string = "Componente de zapatos"
    constructor(
        private _route: ActivatedRoute,
        private _router: Router) {
       }

       ngOnInit(){

        this._route.params.subscribe((params: Params)=>{
            this.marcaPreferida = params.marcaFav;
            console.log(this.marcaPreferida);
      
           if(this.marcaPreferida == 'ninguno')
              this._router.navigate(['/home'])
          });
       }
      
}
