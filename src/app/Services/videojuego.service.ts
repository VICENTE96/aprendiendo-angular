import {Injectable} from '@angular/core';
import {videojuego} from '../models/videojuego';


@Injectable ()
export class VideojuegoService{
    public videojuegos : Array<videojuego>;
constructor(){
    this.videojuegos = [
        new videojuego ('Mario Bros',150,'Niños','Nintendo'),
        new videojuego ('Assasins Creed', 250,'Adultos','X-Box'),
        new videojuego ('Spiderman',100,'General','Play Station')
    ];
}
getVideojuegos():Array <videojuego>{
    return this.videojuegos
}

getTexto(){
    return "Hola Mundo desde un servicio";
}
}