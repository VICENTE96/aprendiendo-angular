import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders,HttpErrorResponse} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()

export class PurificadoraService{

    public urlPuri: string;
    httpOptions={
        headers: new HttpHeaders({
            'Content-Type':'application/json; charset=utf-8'
        })
   

    }
    constructor(public _http: HttpClient){
        
        this.urlPuri = "http://192.168.0.8:1100/WebService1.asmx";
        
        }

        getUser():Observable<any>{
            const body = JSON.stringify({});
            return this._http.post(this.urlPuri+'/GetClientes',body,this.httpOptions)
               
        }

        
}