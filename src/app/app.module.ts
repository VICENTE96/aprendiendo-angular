import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {routing, appRoutingProviders} from './app.routing';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {VideojuegoComponent} from './videojuego/videojuego.component';
import {zapatosComponent} from './zapatos/zapatos.component';
import { AutosComponent } from './autos/autos.component';
import { MotosComponent } from './motos/motos.component';
import { PokemonComponent } from './pokemon/pokemon.component';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ExternoComponent } from './externo/externo.component';
import {HttpClientModule} from '@angular/common/http';
import {CalculadoraPipe} from './pipes/calculadora.pipe';
import { GarrafonesComponent } from './garrafones/garrafones.component'



@NgModule({
  declarations: [
    AppComponent,
    VideojuegoComponent,
    zapatosComponent,
    AutosComponent,
    MotosComponent,
    PokemonComponent,
    HomeComponent,
    NotFoundComponent,
    ExternoComponent,
    CalculadoraPipe,
    GarrafonesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    routing,
    HttpClientModule
  ],
  providers: [
    appRoutingProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
