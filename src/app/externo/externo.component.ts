import { Component, OnInit } from '@angular/core';
import {PeticionesService} from '../Services/peticiones.service';

@Component({
  selector: 'app-externo',
  templateUrl: './externo.component.html',
  styleUrls: ['./externo.component.css'],
  providers:[PeticionesService]
})
export class ExternoComponent implements OnInit {
  public user: any;
  public userId: any;
  public fecha:any;
  constructor(
    private _peticionesService: PeticionesService
  ) { }

  ngOnInit() {
    this.userId = 1;
    this.cargarUsuario();
    
  }

  cargarUsuario(){
    this.fecha = new Date();
    console.log(this.fecha);
    this.user = false;
    this._peticionesService.getUser(this.userId).subscribe(
      result =>{
     //console.log(result);
     this.user = result.data;
      },
      error =>{
        console.log(<any>error);
      }
    );
  }

}
